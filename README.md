# Seattle Curb It Privacy Policy

The Seattle Curb It Alexa skill handles your personal information with care and respect.  By enabling the skill, you agree to the use of information as described in this policy.

With your permission, Seattle Curb It looks up the Alexa host device's street address and uses it for the sole purpose of acquiring your waste collection schedule from Seattle Public Utilities.  No other personal information is collected or used.

Your street address and collection schedule will never be shared outside of Seattle Curb It.
